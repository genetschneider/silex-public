<?php

declare(strict_types = 1);

use Fin\App\AppFactory;

require_once __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::createApp();

$app['debug'] = false;
$app->run();

