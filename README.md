# Silex application 
This application is developed by Genet Schneider as a part of his thesis work.
This application uses [Silex](https://silex.sensiolabs.org) PHP microframework. 

# Installing
Apache server is needed to run this application. Place this app in htdocs/silex. 
localhost:8888/silex/ should work. Use `database-dump.sql` for database reference. This app needs the tables, documented in the file.