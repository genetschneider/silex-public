<?php

declare(strict_types = 1);

namespace Fin\App;

use Fin\App\Controllers\BaseController;
use Silex\Provider as SilexProvider;

final class AppFactory
{
	public static function createApp(): App
	{
		$app = new App();
		$app->registerServices()
			->registerGlobalServiceProviders()
			->registerController('base_controller', BaseController::class)
			->registerRoutes();

		return $app;
	}
}
