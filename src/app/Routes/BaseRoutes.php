<?php

declare(strict_types=1);

namespace Fin\App\Routes;

use Silex\Api\ControllerProviderInterface;
use \Silex\Application;
use Silex\ControllerCollection;

class BaseRoutes implements ControllerProviderInterface
{
	public function connect(Application $app): ControllerCollection
	{
		/** @var \Silex\ControllerCollection $controllers */
		$controllers = $app['controllers_factory'];

		$controllers->get('/', 'base_controller:index');
		$controllers->get('/all-posts', 'base_controller:getAll');
		$controllers->get('/hello/{name}', 'base_controller:hello');
		$controllers->get('/add', 'base_controller:add');
		$controllers->get('/user/{name}', 'base_controller:getUser');
		$controllers->get('/post/{id}', 'base_controller:getPost');

		$controllers->post('/add-post', 'base_controller:addPost');

		return $controllers;
	}
}