<?php

declare(strict_types = 1);

namespace Fin\App\Entity\Hydrator;

use Fin\App\Entity\User;

class UserHydrator
{
	/**
	 * @param array $user
	 * @return User
	 */
	public function hydrate(array $user): User
	{
		$userEntity = new User(
			$user['name'] ?? 'unknown',
			(int)$user['id'] ?? 0
		);

		return $userEntity;
	}
}