<?php

declare(strict_types = 1);

namespace Fin\App;

use Fin\App\Entity\Hydrator\PostHydrator;
use Fin\App\Routes\BaseRoutes;
use Fin\Service\BlogPostService;
use Fin\Service\BlogUserService;
use Fin\App\Entity\Hydrator\UserHydrator;
use Silex\Application;
use Silex\Provider as SilexProvider;

class App extends Application
{
	public function __construct($config = null, $debug = false)
	{
		parent::__construct();

		$this['config'] = $config;
		$this['debug'] = (bool)$debug;
	}

	public function registerController(string $controllerKey, string $controllerClass): App
	{
		$this[$controllerKey] = function($app) use ($controllerClass) {
			return new $controllerClass($app);
		};

		return $this;
	}

	public function registerGlobalServiceProviders(): App
	{
		$this->register(new SilexProvider\HttpFragmentServiceProvider())
			->register(new SilexProvider\ServiceControllerServiceProvider())
			->register(new SilexProvider\RoutingServiceProvider())
			->register(new SilexProvider\TwigServiceProvider(), array(
				'twig.path' => __DIR__ . '/../views')
			)
			->register(new SilexProvider\DoctrineServiceProvider(), [
				'db.options' => $this['silex.db.config'],
			]);

		return $this;
	}

	public function registerServices(): App
	{
		$this['blog.user.service'] = function($app) {
			return new BlogUserService($app['db'], new UserHydrator());
		};

		$this['blog.post.service'] = function($app) {
			return new BlogPostService($app['db'], new PostHydrator());
		};

		// db
		$this['silex.db.config'] = function($app) {
			return [
				'driver' => 'pdo_mysql',
				'host' => 'localhost',
				'port' => 8889,
				'dbname' => 'silex',
				'user' => 'root',
				'password' => 'root',
			];
		};

		return $this;
	}

	public function registerRoutes(): App
	{
		$this->mount('/silex', new BaseRoutes());

		return $this;
	}
}
