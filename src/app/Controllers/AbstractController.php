<?php

declare(strict_types=1);

namespace Fin\App\Controllers;

use Twig_Environment;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController
{
	const TWIG_SUFFIX = '.twig';

	protected function success($data, $status = 200): JsonResponse
	{
		return JsonResponse::create([
			'success' => true,
			'data' => (is_object($data) ? $data->toArray() : $data),
		], $status);
	}

	protected function render($view, Twig_Environment $twig, $data = null, $field = null)
	{
		return $twig->render($view . self::TWIG_SUFFIX, [
			$field => $data
		]);
	}
}
