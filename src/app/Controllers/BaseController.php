<?php

namespace Fin\App\Controllers;

use Fin\Service\BlogPostService;
use Fin\Service\BlogUserService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Twig_Environment;

class BaseController extends AbstractController
{
	const SUCCESS_FIELD = 'success';
	const POSTS_FIELD = 'posts';

	/** @var BlogUserService $blogUserService */
	protected $blogUserService;

	/** @var BlogPostService $blogPostService */
	protected $blogPostService;

	/** @var Twig_Environment */
	protected $twig;

	public function __construct(Application $app)
	{
		$this->blogUserService = $app['blog.user.service'];
		$this->blogPostService = $app['blog.post.service'];
		$this->twig = $app['twig'];
	}

	public function index()
	{
		return $this->render('main', $this->twig);
	}

	public function hello($name, Application $app)
	{
		return "Hey there " . $app->escape($name);
	}

	public function addPost(Request $request, Application $app)
	{
		$name = $app->escape($request->request->get('name'));
		$title = $app->escape($request->request->get('title'));
		$content = $app->escape($request->request->get('content'));

		if (!$name || !$title || !$content) {
			return $this->success(['fail' => true], 204);
		}

		$saveUserId = $this->blogUserService->insertAndReturnId($name);
		$savePost = $this->blogPostService->insert($title, $content, $saveUserId);

		return $this->render('addpost',
			$this->twig,
			($savePost == 1) ? 'Added post!' : 'Something went wrong :(',
			self::SUCCESS_FIELD
		);
	}

	public function getAll()
	{
		$allPosts = $this->blogPostService->getAll();
		return $this->render('posts', $this->twig, $allPosts, self::POSTS_FIELD);
	}

	public function add()
	{
		return $this->render('addpost', $this->twig);
	}

	public function error()
	{
		return $this->render('error', $this->twig);
	}

	public function getUser(string $name)
	{
		return $this->success($this->blogUserService->get($name));
	}

	public function getPost(int $id)
	{
		return $this->success($this->blogPostService->get($id));
	}
}
