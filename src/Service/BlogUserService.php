<?php

declare(strict_types = 1);

namespace Fin\Service;

use Doctrine\DBAL\Connection;
use Fin\App\Entity\User;
use Fin\App\Entity\Hydrator\UserHydrator;

class BlogUserService
{
	const TABLE = 'users';

	/** @var Connection $db */
	protected $db;

	/** @var UserHydrator */
	protected $hydrator;

	public function __construct(Connection $db, UserHydrator $hydrator)
	{
		$this->db = $db;
		$this->hydrator = $hydrator;
	}

	/**
	 * Method to insert user to db and return its id
	 * @param string $name
	 * @return int
	 */
	public function insertAndReturnId(string $name): int
	{
		try {
			$user = $this->get($name);
			$userId = $user->getId();
		} catch (\Exception $e) {
			$user = new User($name);
			$this->db->insert(self::TABLE, $user->getInsertArray());
			$userId = (int)$this->db->lastInsertId();
		}

		return $userId;
	}

	/**
	 *
	 * @param string $name
	 * @return User
	 * @throws \Exception
	 */
	public function get(string $name): User
	{
		$where = [
			'name' => $name,
		];
		$types = [
			'name' => \PDO::PARAM_STR,
		];

		$user = $this->db->fetchAssoc('SELECT * FROM ' . static::TABLE . ' WHERE name = :name LIMIT 1', $where, $types);

		if (!$user) {
			throw new \Exception();
		}

		return $this->hydrator->hydrate($user);
	}
}
