<?php

declare(strict_types = 1);

namespace Fin\Service;

use Doctrine\DBAL\Connection;
use Fin\App\Entity\Hydrator\PostHydrator;
use Fin\App\Entity\Post;

class BlogPostService
{
	const TABLE = 'posts';

	/** @var Connection $db */
	protected $db;

	/** @var PostHydrator $hydrator*/
	protected $hydrator;

	public function __construct(Connection $db, PostHydrator $hydrator)
	{
		$this->db = $db;
		$this->hydrator = $hydrator;
	}

	/**
	 * Inserts post to db
	 * @param string $title
	 * @param string $content
	 * @param int $userId
	 * @return int
	 */
	public function insert(string $title, string $content, int $userId): int
	{
		$post = new Post($title, $content, $userId);

		return $this->db->insert(self::TABLE, $post->getInsertArray());
	}

	/**
	 * Method to return data of all posts
	 * @return array
	 */
	public function getAll(): array
	{
		$sql = 'SELECT * FROM ' . static::TABLE . ' p INNER JOIN users u ON u.id = p.written_by ORDER BY p.id DESC';

		$posts = $this->db->fetchAll($sql);

		return $posts;
	}

	/**
	 * Method to get post data by id
	 * @param int $id
	 * @return Post
	 * @throws \Exception
	 */
	public function get(int $id): Post
	{
		$where = [
			'id' => $id,
		];
		$types = [
			'id' => \PDO::PARAM_INT,
		];

		$post = $this->db->fetchAssoc('SELECT * FROM ' . static::TABLE . ' WHERE id = :id', $where, $types);

		if (!$post) {
			throw new \Exception('Post ' . $id . ' not found.');
		}

		return $this->hydrator->hydrate($post);
	}
}
